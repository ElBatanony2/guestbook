package testNG;

import java.util.concurrent.TimeUnit;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestEntryForm {

    private static WebDriver driver;
    private static String serverBaseURL = System.getProperty("serverBaseURL");
    private static String webpageURI = "/guestbook";

    @BeforeTest
    public static void configureDriver() throws MalformedURLException {
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--window-size=1200x600");

        chromeOptions.setBinary("/usr/bin/google-chrome");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        capability.setPlatform(Platform.LINUX);

        capability.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        driver = new RemoteWebDriver(new URL("http://selenium__standalone-chrome:4444/wd/hub"), capability);
    }

    @Test
    public static void testContent() throws InterruptedException {
        driver.get(serverBaseURL + webpageURI);

        String testName = Long.toHexString(Double.doubleToLongBits(Math.random()));
        String testText = Long.toHexString(Double.doubleToLongBits(Math.random()));

        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(testName);
        driver.findElement(By.xpath("//textarea[@name='text']")).sendKeys(testText);
        driver.findElement(By.xpath("//input[@name='name']")).submit();

        Thread.sleep(1000);
        String expectedString = testText;
        List<WebElement> driverElements = driver.findElements(By.className("card"));
        int entryLength = driverElements.size();
        String lastEntry = driverElements.get(entryLength - 1).getText();

        Assert.assertTrue(lastEntry.contains(testName));
        Assert.assertTrue(lastEntry.contains(testText));

    }


    @AfterSuite
    public static void closeDriver() {
        driver.quit();
    }


}
